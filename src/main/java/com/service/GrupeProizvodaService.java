package com.service;

import java.util.List;
import com.DAO.GrupeArtikalaDAO;
import com.entity.GrupeArtikala;

public class GrupeProizvodaService {
	private GrupeArtikalaDAO grupeArtikala = new GrupeArtikalaDAO();
	
	public List<GrupeArtikala> listGrupeArtikala() {
		return grupeArtikala.listGrupeArtikala();
	}

}
