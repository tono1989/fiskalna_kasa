package com.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class HibernateUtil {

    public static Session openSession() {
    	try {
        	Configuration config = new Configuration();
        	config.configure();
        	ServiceRegistry sr= new ServiceRegistryBuilder().applySettings(config.getProperties()).buildServiceRegistry();  
        	SessionFactory sf=config.buildSessionFactory(sr);
        	return  sf.openSession();
        }
        catch (Throwable ex) {
            // Make sure you log the exception, as it might be swallowed
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
}
