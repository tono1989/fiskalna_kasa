package com.DAO;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import com.entity.GrupeArtikala;
import com.util.HibernateUtil;

public class GrupeArtikalaDAO {
	
	public List<GrupeArtikala> listGrupeArtikala() {
		List<GrupeArtikala> articles = new ArrayList<>();
		Session s = HibernateUtil.openSession();
        s.beginTransaction();
        articles = s.createQuery("from GrupeArtikala").list();
        s.getTransaction().commit();
        s.close();
		return articles;
	}

}
