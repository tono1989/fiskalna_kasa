package com.controller;

import java.util.List;

import com.entity.GrupeArtikala;
import com.service.GrupeProizvodaService;
/**
 * Controller that querries the service layer for products.
 * @author tono
 *
 */
public class ProductGeneratorController {
	final private GrupeProizvodaService grupeProizvodaService = new GrupeProizvodaService();
	
	/**
	 * Get list of products when no querry is given.
	 * @return list of products
	 */
	public List<GrupeArtikala> getProductsList() {
		return grupeProizvodaService.listGrupeArtikala();		
	}
	
	/**
	 * Get list of products when advanced search is used.
	 * @return list of products
	 */
//	public List GetProducts
	
}
