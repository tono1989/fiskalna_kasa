package com;



import java.util.List;

import com.controller.ProductGeneratorController;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.geometry.VerticalDirection;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.ScrollBarBuilder;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.RowConstraints;

import com.entity.GrupeArtikala;
import com.entity.Roba;

public class ProductButtonGenerator {
	@FXML
	private ScrollPane proizvodi;
	@FXML
	private AnchorPane leftPane;
	
	// Reference to main application
	private MainApp mainApp;
	// Controller
	ProductGeneratorController controller = new ProductGeneratorController();
	// Button size parameters:
	final private int buttonWidth = 100;
	final private int buttonHeight = 60;
	final private int rowsInGrid = 5;

	/**
	 * Constructor
	 * Constructor is called before initialize() method.
	 */
	public ProductButtonGenerator() {}
	
	/**
	 * Initializes controller class after the fxml file has been loaded.
	 */
	@FXML
	private void initialize() {
		draw();
	}
	
	/**
	 * Get products from controller and draw product buttons.
	 */
	private void draw() {
		
		proizvodi.setContent(populateHBox(controller.getProductsList()));
	}
	
	/**
	 * Populate grid with button which represent products.
	 */
	private Pane populateHBox(final List products) {
		final GridPane grid = new GridPane();
		grid.setAlignment(Pos. CENTER);
		// Column constraints:
		for (int i = 0; i < rowsInGrid; i++) {
			grid.getColumnConstraints().add(new ColumnConstraints(buttonWidth));
		}
        grid.setHgap(0);
        grid.setVgap(0);
        // Draw them buttons!
        final int prodCnt = products.size();
        final int rowsCnt = (int)prodCnt/rowsInGrid;
		for (int i = 0; i < rowsCnt+ 1; i++) {
			// row constraints:
			grid.getRowConstraints().add(new RowConstraints(buttonHeight));
			for (int j = 0; j < rowsInGrid; j++) {
				int prodNum = i*rowsInGrid + j; // actual product index.
				if (prodNum >= prodCnt) break; // break if all products are in the grid.
//				GrupeArtikala grupe = (GrupeArtikala) products.get(prodNum);
//				final Button prod = new Button(groupOrProd(products.get(prodNum))); // poziv za svaki gumb sporo!
				final ProductButton prod = new ProductButton(products.get(prodNum));
				prod.setText(prod.getName());
				prod.setMaxSize(1000,1000); // make them fill grid cells.
				grid.add(prod, j, i);
			}
		}
		return grid;
	}
	
	private class ProductButton extends Button {
		private Object prod;
		private String name;
//		private Class myClass;
		public ProductButton(final Object o) {
			this.prod = o;
			groupOrProd(prod);
		}
		public String getName() {
			return name;
		}
		/**
		 * Decides are we dealing with general products or concrete instances. 
		 * @param prod
		 * @return
		 */
		private void groupOrProd(final Object prod)	{
			if (prod.getClass() == GrupeArtikala.class) {
				final GrupeArtikala p = (GrupeArtikala) prod;
				name = p.getOznaka();
//				myClass = GrupeArtikala.class;
			} else {
				final Roba p = (Roba) prod;
				name = p.getNaziv();
//				myClass = Roba.class;
			}
		}
		
		private class GrupaArtikalaHandler implements EventHandler<ActionEvent> {

			@Override
			public void handle(ActionEvent arg0) {
				// Querry the database for concrete products.				
			}
			
		}
	}
	
	/**
	 * Decides are we dealing with general products or concrete instances. 
	 * @param prod
	 * @return
	 */
	private String groupOrProd(final Object prod)	{
		if (prod.getClass() == GrupeArtikala.class) {
			final GrupeArtikala p = (GrupeArtikala) prod;
			return p.getOznaka();
		} else {
			final Roba p = (Roba) prod;
			return p.getNaziv();
		}			
	}
	
	/**
	 * Called by the MainApp to give it it's reference to act upon.
	 * 
	 * @param mainApp
	 */
	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
	}
}
