package com.entity;

// Generated Sep 29, 2013 10:47:34 PM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;

/**
 * KalkulacijaStavka generated by hbm2java
 */
public class KalkulacijaStavka implements java.io.Serializable {

	private String kljuc;
	private Roba roba;
	private Kalkulacija kalkulacija;
	private Double ukupno;
	private String mjernaJedinica;
	private Double iznos;
	private Double poreznaStopa;
	private Double fakturnaSaPorezom;
	private Double iznosPoreza;
	private Double zavisniTroskovi;
	private Double kolicina;
	private Double marza;
	private Double iznosRabata;
	private Double cijenaSaPorezom;
	private Double rabat;
	private Double fakturnaVrijednostBezPoreza;
	private Double osnovica;
	private Double cijena;
	private Double cijenaBezPoreza;
	private Double iznosMarze;
	private Set kalkulacijaStavkaKarticas = new HashSet(0);

	public KalkulacijaStavka() {
	}

	public KalkulacijaStavka(String kljuc) {
		this.kljuc = kljuc;
	}

	public KalkulacijaStavka(String kljuc, Roba roba, Kalkulacija kalkulacija,
			Double ukupno, String mjernaJedinica, Double iznos,
			Double poreznaStopa, Double fakturnaSaPorezom, Double iznosPoreza,
			Double zavisniTroskovi, Double kolicina, Double marza,
			Double iznosRabata, Double cijenaSaPorezom, Double rabat,
			Double fakturnaVrijednostBezPoreza, Double osnovica, Double cijena,
			Double cijenaBezPoreza, Double iznosMarze,
			Set kalkulacijaStavkaKarticas) {
		this.kljuc = kljuc;
		this.roba = roba;
		this.kalkulacija = kalkulacija;
		this.ukupno = ukupno;
		this.mjernaJedinica = mjernaJedinica;
		this.iznos = iznos;
		this.poreznaStopa = poreznaStopa;
		this.fakturnaSaPorezom = fakturnaSaPorezom;
		this.iznosPoreza = iznosPoreza;
		this.zavisniTroskovi = zavisniTroskovi;
		this.kolicina = kolicina;
		this.marza = marza;
		this.iznosRabata = iznosRabata;
		this.cijenaSaPorezom = cijenaSaPorezom;
		this.rabat = rabat;
		this.fakturnaVrijednostBezPoreza = fakturnaVrijednostBezPoreza;
		this.osnovica = osnovica;
		this.cijena = cijena;
		this.cijenaBezPoreza = cijenaBezPoreza;
		this.iznosMarze = iznosMarze;
		this.kalkulacijaStavkaKarticas = kalkulacijaStavkaKarticas;
	}

	public String getKljuc() {
		return this.kljuc;
	}

	public void setKljuc(String kljuc) {
		this.kljuc = kljuc;
	}

	public Roba getRoba() {
		return this.roba;
	}

	public void setRoba(Roba roba) {
		this.roba = roba;
	}

	public Kalkulacija getKalkulacija() {
		return this.kalkulacija;
	}

	public void setKalkulacija(Kalkulacija kalkulacija) {
		this.kalkulacija = kalkulacija;
	}

	public Double getUkupno() {
		return this.ukupno;
	}

	public void setUkupno(Double ukupno) {
		this.ukupno = ukupno;
	}

	public String getMjernaJedinica() {
		return this.mjernaJedinica;
	}

	public void setMjernaJedinica(String mjernaJedinica) {
		this.mjernaJedinica = mjernaJedinica;
	}

	public Double getIznos() {
		return this.iznos;
	}

	public void setIznos(Double iznos) {
		this.iznos = iznos;
	}

	public Double getPoreznaStopa() {
		return this.poreznaStopa;
	}

	public void setPoreznaStopa(Double poreznaStopa) {
		this.poreznaStopa = poreznaStopa;
	}

	public Double getFakturnaSaPorezom() {
		return this.fakturnaSaPorezom;
	}

	public void setFakturnaSaPorezom(Double fakturnaSaPorezom) {
		this.fakturnaSaPorezom = fakturnaSaPorezom;
	}

	public Double getIznosPoreza() {
		return this.iznosPoreza;
	}

	public void setIznosPoreza(Double iznosPoreza) {
		this.iznosPoreza = iznosPoreza;
	}

	public Double getZavisniTroskovi() {
		return this.zavisniTroskovi;
	}

	public void setZavisniTroskovi(Double zavisniTroskovi) {
		this.zavisniTroskovi = zavisniTroskovi;
	}

	public Double getKolicina() {
		return this.kolicina;
	}

	public void setKolicina(Double kolicina) {
		this.kolicina = kolicina;
	}

	public Double getMarza() {
		return this.marza;
	}

	public void setMarza(Double marza) {
		this.marza = marza;
	}

	public Double getIznosRabata() {
		return this.iznosRabata;
	}

	public void setIznosRabata(Double iznosRabata) {
		this.iznosRabata = iznosRabata;
	}

	public Double getCijenaSaPorezom() {
		return this.cijenaSaPorezom;
	}

	public void setCijenaSaPorezom(Double cijenaSaPorezom) {
		this.cijenaSaPorezom = cijenaSaPorezom;
	}

	public Double getRabat() {
		return this.rabat;
	}

	public void setRabat(Double rabat) {
		this.rabat = rabat;
	}

	public Double getFakturnaVrijednostBezPoreza() {
		return this.fakturnaVrijednostBezPoreza;
	}

	public void setFakturnaVrijednostBezPoreza(
			Double fakturnaVrijednostBezPoreza) {
		this.fakturnaVrijednostBezPoreza = fakturnaVrijednostBezPoreza;
	}

	public Double getOsnovica() {
		return this.osnovica;
	}

	public void setOsnovica(Double osnovica) {
		this.osnovica = osnovica;
	}

	public Double getCijena() {
		return this.cijena;
	}

	public void setCijena(Double cijena) {
		this.cijena = cijena;
	}

	public Double getCijenaBezPoreza() {
		return this.cijenaBezPoreza;
	}

	public void setCijenaBezPoreza(Double cijenaBezPoreza) {
		this.cijenaBezPoreza = cijenaBezPoreza;
	}

	public Double getIznosMarze() {
		return this.iznosMarze;
	}

	public void setIznosMarze(Double iznosMarze) {
		this.iznosMarze = iznosMarze;
	}

	public Set getKalkulacijaStavkaKarticas() {
		return this.kalkulacijaStavkaKarticas;
	}

	public void setKalkulacijaStavkaKarticas(Set kalkulacijaStavkaKarticas) {
		this.kalkulacijaStavkaKarticas = kalkulacijaStavkaKarticas;
	}

}
