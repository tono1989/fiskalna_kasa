package com.entity;

// Generated Sep 29, 2013 10:47:34 PM by Hibernate Tools 3.4.0.CR1

/**
 * ArtikalNormativi generated by hbm2java
 */
public class ArtikalNormativi implements java.io.Serializable {

	private String id;
	private Roba robaByNormativKljuc;
	private Roba robaByArtikalKljuc;
	private Double kolicina;

	public ArtikalNormativi() {
	}

	public ArtikalNormativi(String id) {
		this.id = id;
	}

	public ArtikalNormativi(String id, Roba robaByNormativKljuc,
			Roba robaByArtikalKljuc, Double kolicina) {
		this.id = id;
		this.robaByNormativKljuc = robaByNormativKljuc;
		this.robaByArtikalKljuc = robaByArtikalKljuc;
		this.kolicina = kolicina;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Roba getRobaByNormativKljuc() {
		return this.robaByNormativKljuc;
	}

	public void setRobaByNormativKljuc(Roba robaByNormativKljuc) {
		this.robaByNormativKljuc = robaByNormativKljuc;
	}

	public Roba getRobaByArtikalKljuc() {
		return this.robaByArtikalKljuc;
	}

	public void setRobaByArtikalKljuc(Roba robaByArtikalKljuc) {
		this.robaByArtikalKljuc = robaByArtikalKljuc;
	}

	public Double getKolicina() {
		return this.kolicina;
	}

	public void setKolicina(Double kolicina) {
		this.kolicina = kolicina;
	}

}
