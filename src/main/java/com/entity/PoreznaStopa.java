package com.entity;

// Generated Sep 29, 2013 10:47:34 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * PoreznaStopa generated by hbm2java
 */
public class PoreznaStopa implements java.io.Serializable {

	private String kljuc;
	private Double iznos;
	private String opis;
	private Date pocetak;
	private Date kraj;
	private Set robasForPdvNabava = new HashSet(0);
	private Set robasForPot = new HashSet(0);
	private Set robasForPdv = new HashSet(0);

	public PoreznaStopa() {
	}

	public PoreznaStopa(String kljuc) {
		this.kljuc = kljuc;
	}

	public PoreznaStopa(String kljuc, Double iznos, String opis, Date pocetak,
			Date kraj, Set robasForPdvNabava, Set robasForPot, Set robasForPdv) {
		this.kljuc = kljuc;
		this.iznos = iznos;
		this.opis = opis;
		this.pocetak = pocetak;
		this.kraj = kraj;
		this.robasForPdvNabava = robasForPdvNabava;
		this.robasForPot = robasForPot;
		this.robasForPdv = robasForPdv;
	}

	public String getKljuc() {
		return this.kljuc;
	}

	public void setKljuc(String kljuc) {
		this.kljuc = kljuc;
	}

	public Double getIznos() {
		return this.iznos;
	}

	public void setIznos(Double iznos) {
		this.iznos = iznos;
	}

	public String getOpis() {
		return this.opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public Date getPocetak() {
		return this.pocetak;
	}

	public void setPocetak(Date pocetak) {
		this.pocetak = pocetak;
	}

	public Date getKraj() {
		return this.kraj;
	}

	public void setKraj(Date kraj) {
		this.kraj = kraj;
	}

	public Set getRobasForPdvNabava() {
		return this.robasForPdvNabava;
	}

	public void setRobasForPdvNabava(Set robasForPdvNabava) {
		this.robasForPdvNabava = robasForPdvNabava;
	}

	public Set getRobasForPot() {
		return this.robasForPot;
	}

	public void setRobasForPot(Set robasForPot) {
		this.robasForPot = robasForPot;
	}

	public Set getRobasForPdv() {
		return this.robasForPdv;
	}

	public void setRobasForPdv(Set robasForPdv) {
		this.robasForPdv = robasForPdv;
	}

}
