package com;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import com.entity.GrupeArtikala;
import com.util.HibernateUtil;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class MainApp extends Application {
	private Stage primaryStage;
	private BorderPane rootLayout;
	
	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("Fiskalna kasa");
        try {
        	// Load the root layout from the fxml file
        	FXMLLoader loader = new FXMLLoader(MainApp.class.getResource("./view/RootLayout.fxml"));
        	rootLayout = (BorderPane) loader.load();
        	Scene scene = new Scene(rootLayout);
        	primaryStage.setScene(scene);
        	primaryStage.show();	          
        } catch (IOException e) {
        	// Exception gets thrown if the fxml file could not be loaded
        	e.printStackTrace();
        }
		showUnosRacuna();
	}

	/**
	 * Returns the main stage.
	 * @return
	 */
	public Stage getPrimaryStage() {
		return primaryStage;
	}	
		
	/**
	 * Shows the person overview scene.
	 */
	public void showUnosRacuna() {
		try {
			// Load the fxml file and set into the center of the main layout
			FXMLLoader loader = new FXMLLoader(MainApp.class.getResource("./view/UnosRacuna.fxml"));
			AnchorPane overviewPage = (AnchorPane) loader.load();
			rootLayout.setCenter(overviewPage);
			
			// Give controller class access to the main app.
			ProductButtonGenerator controller = loader.getController();
			controller.setMainApp(this);	
			
		} catch (IOException e) {
			// Exception gets thrown if the fxml file could not be loaded
			e.printStackTrace();
		}	
	}
	
	static public void test() {
		List<GrupeArtikala> list=new ArrayList<>();
        Session s=HibernateUtil.openSession();
        s.beginTransaction();
        list=s.createQuery("from GrupeArtikala").list();
        s.getTransaction().commit();
        s.close();
        System.out.println(list.get(0).getOznaka());
	}
	
	public static void main(String[] args) {
		launch(args);
		//test();
	}
}
